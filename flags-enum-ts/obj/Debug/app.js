﻿var Flagged = require("./FlaggedEnum");

var FlatTestEnum;
(function (FlatTestEnum) {
    FlatTestEnum[FlatTestEnum["_1_0"] = 1 << 0] = "_1_0";
    FlatTestEnum[FlatTestEnum["_1_1"] = 1 << 1] = "_1_1";
    FlatTestEnum[FlatTestEnum["_1_2"] = 1 << 2] = "_1_2";
    FlatTestEnum[FlatTestEnum["_1_3"] = 1 << 3] = "_1_3";
    FlatTestEnum[FlatTestEnum["_1_4"] = 1 << 4] = "_1_4";
    FlatTestEnum[FlatTestEnum["_1_18"] = 1 << 18] = "_1_18";
})(FlatTestEnum || (FlatTestEnum = {}));

var FlatTest = Flagged.FlaggedEnum.create(FlatTestEnum, 1 << 18);

var value = new FlatTest(1);
var _value = new FlatTest(1);
var value2 = new FlatTest("_1_1");
var value3 = new FlatTest(5);

console.log(value);
console.log(value.toString());

console.log(value2);
console.log(value2.toString());

console.log(value3);
console.log(value3.toString());

console.assert(value3.contains(value), "contains test");

console.log(value.add(value2));
console.log(value3.remove(value));
console.log(value3.intersect(value));

console.log(value3.toArray());
console.assert(value.equals(1), "equals 1 test");
console.assert(value.equals("_1_0"), "equals _1_0 test");
console.assert(value.equals(2) === false, "not equals 2 test");
console.assert(value.equals("_1_1") === false, "not equals _1_1 test");

// expected to fail
console.assert(value == _value, "equals 1 (flaggedenum) test");
//# sourceMappingURL=app.js.map
